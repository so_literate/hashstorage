package hashstorage_test

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/so_literate/hashstorage"
)

func Example() {
	root := filepath.Join(os.TempDir(), "hashstorage")

	s, err := hashstorage.New(root)
	if err != nil {
		fmt.Println("hashstorage.New:", err)
		return
	}

	buf := bytes.NewBuffer([]byte("file's content\n"))

	fileName, err := s.SaveFile(buf, hashstorage.WithExt(".txt"))
	if err != nil {
		fmt.Println("s.SaveFile:", err)
		return
	}

	fmt.Println("fileName:", fileName)

	fileToPath, err := s.GetPathToFile(fileName)
	if err != nil {
		fmt.Println("s.GetPathToFile:", err)
		return
	}

	// In Unix system fileToPath is /tmp/hashstorage/13/1334...a2aa.txt
	if !strings.Contains(fileToPath, filepath.Join(root, "13", fileName)) {
		fmt.Println("wrong path:", fileToPath)
	}

	file, err := s.Open(fileName)
	if err != nil {
		fmt.Println("s.Open:", err)
		return
	}

	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		fmt.Println("io.ReadAll:", err)
		return
	}

	fmt.Println("content:", string(content))

	err = s.Remove(fileName)
	if err != nil {
		fmt.Println(err)
		return
	}

	if err = os.RemoveAll(root); err != nil {
		fmt.Println("os.RemoveAll: %w", err)
	}

	// Output:
	// fileName: 1334de9213a8062957171c1da0ba643b727a16dc59e2fa7f3705bc88b67ca2aa.txt
	// content: file's content
}
