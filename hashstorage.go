// Package hashstorage is a file storage.
// It use hash of the file to save files.
// 2 first symbols of the hash are name of the file's directory.
// Dir tree example:
//   /
//   /00/
//   | - /00hashofthefile
//   /01/
//   | - /01hashofthefile
//   | - /01anotherhash
//   ...
//   /ff/
// Each directory is a prefix of the file name.
package hashstorage

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"hash"
	"io"
	"io/fs"
	"os"
	"path/filepath"
)

var (
	// ErrRootDirIsEmpty returns when you try to call New method with empty root dir.
	ErrRootDirIsEmpty = errors.New("root dir is empty")
	// ErrFileNameIsTooShort returns when you try to use wrong file name.
	ErrFileNameIsTooShort = errors.New("file name is too short")
)

// HashStorage contains methods to Save and Open files in file system.
// All files are organized by hash prefix.
type HashStorage struct {
	NewHash               func() hash.Hash
	RootDir               string
	NoAutoMigrate         bool
	StoreDirPermission    os.FileMode
	DefualtFilePermission os.FileMode
}

// Config is an optional config of the HashStorage initializer.
type Config func(*HashStorage)

// ConfigHashFunc is a setter of the Hash func to the storage.
// Default value is the sha256.New func.
// Example:
//   hashstorage.ConfigHashFunc(sha1.New)
func ConfigHashFunc(hashFunc func() hash.Hash) Config {
	return func(s *HashStorage) { s.NewHash = hashFunc }
}

// ConfigNoAutoMigrate migration method will be disabled.
// You must call it manually.
//   s.Migrate()
func ConfigNoAutoMigrate() Config {
	return func(s *HashStorage) { s.NoAutoMigrate = true }
}

// ConfigStoreDirPermission sets file mod of the storage directories.
// 0o755 (rwxr-xr-x) by default.
func ConfigStoreDirPermission(storeDirPermission os.FileMode) Config {
	return func(s *HashStorage) { s.StoreDirPermission = storeDirPermission }
}

// ConfigDefualtFilePermission sets default file permission.
// You can set file permission for special file with WithFilePermission method.
// 0o644 (rw-r--r--) by default.
func ConfigDefualtFilePermission(defualtFilePermission os.FileMode) Config {
	return func(s *HashStorage) { s.DefualtFilePermission = defualtFilePermission }
}

// New creates new file storage with optional config.
// It will create a directory tree by default.
func New(rootDir string, configs ...Config) (*HashStorage, error) {
	if rootDir == "" {
		return nil, ErrRootDirIsEmpty
	}

	s := HashStorage{
		RootDir:               rootDir,
		NewHash:               sha256.New,
		NoAutoMigrate:         false,
		StoreDirPermission:    0o755, // rwxr-xr-x
		DefualtFilePermission: 0o644, // rw-r--r--
	}

	for _, conf := range configs {
		conf(&s)
	}

	if !s.NoAutoMigrate {
		if err := s.Migrate(); err != nil {
			return nil, fmt.Errorf("s.Migrate: %w", err)
		}
	}

	return &s, nil
}

// Migrate method to create a directory tree of the storage manually.
// You don't have to call it if you are not disabled it in the "New" method (ConfigNoAutoMigrate).
func (s *HashStorage) Migrate() error {
	const hextable = "0123456789abcdef"

	hexSymbols := make([]string, len(hextable))
	for i, v := range hextable {
		hexSymbols[i] = string(v)
	}

	dirList := make([]string, 256)
	i := 0

	for _, symbolLeft := range hexSymbols {
		for _, symbolRight := range hexSymbols {
			dirList[i] = filepath.Join(s.RootDir, symbolLeft+symbolRight)
			i++
		}
	}

	err := os.MkdirAll(s.RootDir, s.StoreDirPermission)
	if err != nil {
		return fmt.Errorf("os.MkdirAll RootDir: %w", err)
	}

	for _, dir := range dirList {
		err = os.Mkdir(dir, s.StoreDirPermission)
		if err == nil || errors.Is(err, os.ErrExist) {
			continue
		}

		return fmt.Errorf("os.Mkdir: %w", err)
	}

	return nil
}

// Options is parameters of the GetFileName and SaveFile methods.
type Options struct {
	FileName        string
	Ext             string
	CheckFileExists bool
	FilePermission  os.FileMode
}

// WithOption provides an option setter.
type WithOption func(opt *Options)

// WithFileName allows you save file with your name.
// For example you can use it when you already know the file's hash.
func WithFileName(fileName string) WithOption {
	return func(opt *Options) { opt.FileName = fileName }
}

// WithExt sets your custom extension of the file.
// You can use it with dot or without dot.
//   hashstorage.WithExt("txt") || hashstorage.WithExt(".txt")
// By default storage saves file without extension just file's hash.
func WithExt(ext string) WithOption {
	return func(opt *Options) {
		if ext == "" {
			opt.Ext = ext
			return
		}

		if ext[0] != '.' {
			ext = "." + ext
		}

		opt.Ext = ext
	}
}

// WithCheckFileExists methods will check file in the file storage.
// Method will return os.ErrExist if file already exist.
func WithCheckFileExists() WithOption {
	return func(opt *Options) { opt.CheckFileExists = true }
}

// WithFilePermission parameter for SaveFile only.
// It provides to save file with special permission.
func WithFilePermission(filePermission os.FileMode) WithOption {
	return func(opt *Options) { opt.FilePermission = filePermission }
}

func (s *HashStorage) getOptions(opts ...WithOption) Options {
	opt := Options{
		FileName:        "",
		Ext:             "",
		CheckFileExists: false,
		FilePermission:  s.DefualtFilePermission,
	}

	for _, o := range opts {
		o(&opt)
	}

	return opt
}

// GetPathToFile returns full path of the file in the hash storage.
// It just generates the path without validation.
func (s *HashStorage) GetPathToFile(fileName string) (string, error) {
	if len(fileName) < 2 {
		return "", fmt.Errorf("%w: %q", ErrFileNameIsTooShort, fileName)
	}

	return filepath.Join(s.RootDir, fileName[:2], fileName), nil
}

func (s *HashStorage) getFileName(file io.Reader, ext string) (string, error) {
	h := s.NewHash()

	if _, err := io.Copy(h, file); err != nil {
		return "", fmt.Errorf("write file to hash func: %w", err)
	}

	fileHash := hex.EncodeToString(h.Sum(nil))
	fileName := fileHash + ext

	return fileName, nil
}

func (s *HashStorage) checkFileExists(fileName string, checkFileExists bool) error {
	if !checkFileExists {
		return nil
	}

	pathToFile, err := s.GetPathToFile(fileName)
	if err != nil {
		return fmt.Errorf("s.GetPathToFile: %w", err)
	}

	_, err = os.Stat(pathToFile)
	if err == nil {
		return fmt.Errorf("%w: %q", os.ErrExist, fileName)
	}

	if errors.Is(err, os.ErrNotExist) {
		return nil
	}

	return fmt.Errorf("os.Stat %q: %w", fileName, err)
}

// GetFileName calculates the hash of a file and returns it name.
// You can add extension of the file and checker of the file in the storage.
// If you plan to save this file you must to create copy of it, because this method will
// read io.Reader and you can't use it again. You can use TeeReader.
//   file = io.TeeReader(file, copy)
// Also you can skip calculate step in the SaveFile method using WithFileName option.
func (s *HashStorage) GetFileName(file io.Reader, opts ...WithOption) (string, error) {
	opt := s.getOptions(opts...)

	if opt.FileName == "" {
		var err error

		opt.FileName, err = s.getFileName(file, opt.Ext)
		if err != nil {
			return "", fmt.Errorf("getFileName: %w", err)
		}
	}

	if err := s.checkFileExists(opt.FileName, opt.CheckFileExists); err != nil {
		return opt.FileName, fmt.Errorf("checkFileExists: %w", err)
	}

	return opt.FileName, nil
}

func (s *HashStorage) writeFile(file io.Reader, fileName string, perm os.FileMode) error {
	pathToFile, err := s.GetPathToFile(fileName)
	if err != nil {
		return fmt.Errorf("s.GetPathToFile: %w", err)
	}

	openedFile, err := os.OpenFile(pathToFile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, perm)
	if err != nil {
		return fmt.Errorf("os.OpenFile: %w", err)
	}

	defer openedFile.Close() // nolint:errcheck // don't care about it.

	if err = openedFile.Truncate(0); err != nil {
		return fmt.Errorf("openedFile.Truncate: %w", err)
	}

	if _, err = openedFile.Seek(0, 0); err != nil {
		return fmt.Errorf("openedFile.Seek: %w", err)
	}

	if _, err = io.Copy(openedFile, file); err != nil {
		return fmt.Errorf("write file to os: %w", err)
	}

	return nil
}

// SaveFile saves file in the storage.
// It can calculate hash of the file but you can use your own hash name with WithFileName option.
// Returns name of the saved file.
// See WithOption methods to add extension of the file or file checker and etc.
func (s *HashStorage) SaveFile(file io.Reader, opts ...WithOption) (string, error) {
	opt := s.getOptions(opts...)

	var fileToSave io.Reader

	if opt.FileName == "" {
		buf := bytes.Buffer{}
		fileToSave = &buf
		file = io.TeeReader(file, &buf)
		var err error

		opt.FileName, err = s.getFileName(file, opt.Ext)
		if err != nil {
			return "", fmt.Errorf("getFileName: %w", err)
		}

	} else {
		fileToSave = file
	}

	if err := s.checkFileExists(opt.FileName, opt.CheckFileExists); err != nil {
		return opt.FileName, fmt.Errorf("checkFileExists: %w", err)
	}

	err := s.writeFile(fileToSave, opt.FileName, opt.FilePermission)
	if err != nil {
		return opt.FileName, fmt.Errorf("writeFile: %w", err)
	}

	return opt.FileName, nil
}

var _ fs.FS = &HashStorage{}

// Open returns a file from storage by filename.
// It returns os.ErrNotExist if the file not exists.
func (s *HashStorage) Open(fileName string) (fs.File, error) {
	pathToFile, err := s.GetPathToFile(fileName)
	if err != nil {
		return nil, fmt.Errorf("s.GetPathToFile: %w", err)
	}

	file, err := os.Open(pathToFile)
	if err != nil {
		return nil, fmt.Errorf("os.Open: %w", err)
	}

	return file, nil
}

// Remove removes a file from storage by filename.
// It returns os.ErrNotExist if the file not exists.
func (s *HashStorage) Remove(fileName string) error {
	pathToFile, err := s.GetPathToFile(fileName)
	if err != nil {
		return fmt.Errorf("s.GetPathToFile: %w", err)
	}

	err = os.Remove(pathToFile)
	if err != nil {
		return fmt.Errorf("os.Remove: %w", err)
	}

	return nil
}
