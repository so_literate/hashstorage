HashStorage
========

It is a file storage that keeps files organized by hash prefix (git/go-build cache).

```
/root/
   /00/
   | - /00hashofthefile
   /01/
   | - /01hashofthefile
   | - /01anotherhash
   /de/
   | - /deadbeaf.txt
   ...
   /ff/
```

# Example

```go
import "gitlab.com/so_literate/hashstorage"

func main() {
	root := filepath.Join(os.TempDir(), "hashstorage")

	s, err := hashstorage.New(root)
	if err != nil {
		fmt.Println("hashstorage.New:", err)
		return
	}

	buf := bytes.NewBuffer([]byte("file's content\n"))

	fileName, err := s.SaveFile(buf, hashstorage.WithExt(".txt"))
	if err != nil {
		fmt.Println("s.SaveFile:", err)
		return
	}

	// In Unix system file to path is /tmp/hashstorage/13/1334...a2aa.txt

	file, err := s.Open(fileName)
	if err != nil {
		fmt.Println("s.Open:", err)
		return
	}

	defer file.Close()

	err = s.Remove(fileName)
	if err != nil {
		fmt.Println(err)
		return
	}
}
```
