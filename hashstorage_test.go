package hashstorage_test

import (
	"bytes"
	"crypto/sha1"
	"errors"
	"fmt"
	"hash"
	"io"
	"math/big"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/so_literate/hashstorage"
)

var errTestError = errors.New("test error")

type badHash struct{}

func newBadHash() hash.Hash {
	return &badHash{}
}

func (*badHash) Write(p []byte) (n int, err error) { return 0, errTestError }
func (*badHash) Sum(b []byte) []byte               { return b }
func (*badHash) Reset()                            {}
func (*badHash) Size() int                         { return 0 }
func (*badHash) BlockSize() int                    { return 0 }

type badFile struct{}

func (*badFile) Read(p []byte) (n int, err error) { return 0, errTestError }

func TestNew(t *testing.T) {
	t.Parallel()
	root := filepath.Join(t.TempDir(), "New")

	s, err := hashstorage.New(
		root,
		hashstorage.ConfigHashFunc(sha1.New),
		hashstorage.ConfigNoAutoMigrate(),
		hashstorage.ConfigStoreDirPermission(0o777),    // rwxrwxrwx
		hashstorage.ConfigDefualtFilePermission(0o666), // rw-rw-r-w
	)
	if err != nil {
		t.Fatal(err)
	}

	got := fmt.Sprintf("%#v", s)

	want := fmt.Sprintf("%#v", &hashstorage.HashStorage{
		RootDir:               root,
		NewHash:               sha1.New,
		NoAutoMigrate:         true,
		StoreDirPermission:    0o777,
		DefualtFilePermission: 0o666,
	})

	if want != got {
		t.Fatalf("not equal:\n%s\n%s", want, got)
	}

	// Default settings.
	_, err = hashstorage.New(root)
	if err != nil {
		t.Fatal(err)
	}

	// With empty root dir.
	_, err = hashstorage.New("")
	if !errors.Is(err, hashstorage.ErrRootDirIsEmpty) {
		t.Fatal(err)
	}
}

func TestHashStorage_Migrate(t *testing.T) {
	t.Parallel()
	root := filepath.Join(t.TempDir(), "Migrate")

	assertRootDir := func(dirName string, wantErr error) {
		t.Helper()

		_, err := os.Stat(filepath.Join(root, dirName))
		if !errors.Is(err, wantErr) {
			t.Fatal("wrong error:", err)
		}
	}

	assertRoot := func(wantDirs int) {
		t.Helper()

		dirs, err := os.ReadDir(root)
		if err != nil {
			t.Fatal("os.ReadDir", err)
		}

		gotDirs := len(dirs)

		if wantDirs != gotDirs {
			t.Fatal("wrong number of dirs", wantDirs, gotDirs)
		}
	}

	s, err := hashstorage.New(root, hashstorage.ConfigNoAutoMigrate())
	if err != nil {
		t.Fatal(err)
	}

	assertRootDir(root, os.ErrNotExist)

	err = s.Migrate()
	if err != nil {
		t.Fatal(err)
	}

	assertRootDir("", nil)
	assertRoot(256)

	// It's safe to call twice.
	err = s.Migrate()
	if err != nil {
		t.Fatal(err)
	}

	assertRootDir("", nil)
	assertRoot(256)

	assertRootDir("00", nil)
	assertRootDir("ff", nil)
	assertRootDir("er", os.ErrNotExist)

	dirs, err := os.ReadDir(root)
	if err != nil {
		t.Fatal(err)
	}

	for want, dir := range dirs {
		dirInt, ok := big.NewInt(0).SetString(dir.Name(), 16)
		if !ok {
			t.Fatal(dir.Name())
		}

		got := int(dirInt.Int64())

		if want != got {
			t.Fatal("wrong number", want, got)
		}
	}

	// Bad settings.
	_, err = hashstorage.New(filepath.Join(t.TempDir(), "bad"), hashstorage.ConfigStoreDirPermission(0))
	if !errors.Is(err, os.ErrPermission) {
		t.Fatal()
	}

	// Bad root's parent dir.
	badParent := t.TempDir()

	if err = os.Chmod(badParent, 0); err != nil {
		t.Fatal(err)
	}

	_, err = hashstorage.New(filepath.Join(badParent, "bad"))
	if !errors.Is(err, os.ErrPermission) {
		t.Fatal()
	}
}

func TestHashStorage_GetPathToFile(t *testing.T) {
	t.Parallel()
	root := filepath.Join(t.TempDir(), "GetPathToFile")

	s, err := hashstorage.New(root)
	if err != nil {
		t.Fatal(err)
	}

	fileName := "1334de9213a8062957171c1da0ba643b727a16dc59e2fa7f3705bc88b67ca2aa.txt"

	got, err := s.GetPathToFile(fileName)
	if err != nil {
		t.Fatal(err)
	}

	want := filepath.Join(root, "13", fileName)
	if want != got {
		t.Fatal("wrong path:", want, got)
	}

	_, err = s.GetPathToFile("f")
	if !errors.Is(err, hashstorage.ErrFileNameIsTooShort) {
		t.Fatal(err)
	}
}

func TestHashStorage_GetFileName(t *testing.T) {
	t.Parallel()
	root := filepath.Join(t.TempDir(), "GetFileName")

	s, err := hashstorage.New(root)
	if err != nil {
		t.Fatal(err)
	}

	testCase := func(wantErr error, wantFile string, opt ...hashstorage.WithOption) {
		t.Helper()

		got, err := s.GetFileName(bytes.NewBufferString("file\n"), opt...)
		if !errors.Is(err, wantErr) {
			t.Fatalf("wrong error:\n%v\n%v", wantErr, err)
		}

		if wantFile != got {
			t.Fatalf("wrong file name:\n%q\n%q", wantFile, got)
		}
	}

	testCase(nil, "8b911a8716b94442f9ca3dff20584048536e4c2f47b8b5bb9096cbd43c3432d5")

	testCase(
		nil,
		"8b911a8716b94442f9ca3dff20584048536e4c2f47b8b5bb9096cbd43c3432d5",
		hashstorage.WithExt(""),
	)

	testCase(
		nil,
		"8b911a8716b94442f9ca3dff20584048536e4c2f47b8b5bb9096cbd43c3432d5.txt",
		hashstorage.WithExt("txt"),
	)

	testCase(
		nil,
		"8b911a8716b94442f9ca3dff20584048536e4c2f47b8b5bb9096cbd43c3432d5.txt",
		hashstorage.WithExt(".txt"),
	)

	testCase(
		nil,
		"8b911a8716b94442f9ca3dff20584048536e4c2f47b8b5bb9096cbd43c3432d5.test",
		hashstorage.WithExt(".test"), hashstorage.WithCheckFileExists(),
	)

	err = os.WriteFile(
		filepath.Join(root, "8b", "8b911a8716b94442f9ca3dff20584048536e4c2f47b8b5bb9096cbd43c3432d5.test"),
		[]byte("file\n"),
		s.DefualtFilePermission,
	)
	if err != nil {
		t.Fatal(err)
	}

	testCase(
		os.ErrExist,
		"8b911a8716b94442f9ca3dff20584048536e4c2f47b8b5bb9096cbd43c3432d5.test",
		hashstorage.WithExt(".test"), hashstorage.WithCheckFileExists(),
	)

	testCase(
		nil,
		"filename",
		hashstorage.WithFileName("filename"), hashstorage.WithExt(".txt"),
	)

	// Broken file dir.
	if err = os.Chmod(filepath.Join(root, "8b"), 0); err != nil {
		t.Fatal(err)
	}

	testCase(
		os.ErrPermission,
		"8b911a8716b94442f9ca3dff20584048536e4c2f47b8b5bb9096cbd43c3432d5",
		hashstorage.WithCheckFileExists(),
	)

	if err = os.Chmod(filepath.Join(root, "8b"), s.StoreDirPermission); err != nil { // Must to return.
		t.Fatal(err)
	}

	// Change hash func.
	s.NewHash = sha1.New
	testCase(nil, "046c168df2244d3a13985f042a50e479fe56455e")

	s.NewHash = newBadHash
	testCase(errTestError, "")

	// Bad custom file name.
	testCase(
		hashstorage.ErrFileNameIsTooShort,
		"f",
		hashstorage.WithFileName("f"), hashstorage.WithCheckFileExists(),
	)
}

func TestHashStorage_SaveFile(t *testing.T) {
	t.Parallel()
	root := filepath.Join(t.TempDir(), "SaveFile")

	s, err := hashstorage.New(root)
	if err != nil {
		t.Fatal(err)
	}

	testCase := func(wantErr error, wantFile string, opt ...hashstorage.WithOption) {
		t.Helper()

		content := []byte("saved_file\n")

		got, err := s.SaveFile(bytes.NewBuffer(content), opt...)
		if !errors.Is(err, wantErr) {
			t.Fatalf("wrong error:\n%v\n%v", wantErr, err)
		}

		if wantFile != got {
			t.Fatalf("wrong file name:\n%q\n%q", wantFile, got)
		}

		if err != nil {
			return
		}

		path, err := s.GetPathToFile(got)
		if err != nil {
			t.Fatal("GetPathToFile:", err)
		}

		savedFile, err := os.ReadFile(path)
		if err != nil {
			t.Fatal("os.ReadFile:", err)
		}

		if !bytes.Equal(content, savedFile) {
			t.Fatalf("!bytes.Equal\n%q\n%q", content, savedFile)
		}
	}

	testCase(nil, "543cc8ead1ffe2388a080b66b40a337a9356089fbf90315148d4073ff0940b71")

	testCase(
		os.ErrExist,
		"543cc8ead1ffe2388a080b66b40a337a9356089fbf90315148d4073ff0940b71",
		hashstorage.WithCheckFileExists(),
	)

	testCase(
		nil,
		"543cc8ead1ffe2388a080b66b40a337a9356089fbf90315148d4073ff0940b71.txt",
		hashstorage.WithCheckFileExists(), hashstorage.WithExt("txt"),
	)

	testCase(
		nil,
		"543cc8ead1ffe2388a080b66b40a337a9356089fbf90315148d4073ff0940b71.prms",
		hashstorage.WithExt("prms"), hashstorage.WithFilePermission(0o600),
	)

	path, _ := s.GetPathToFile("543cc8ead1ffe2388a080b66b40a337a9356089fbf90315148d4073ff0940b71.prms")
	info, err := os.Stat(path)
	if err != nil {
		t.Fatal(err)
	}

	if info.Mode() != 0o600 {
		t.Fatal("wrong file mode:", info.Mode().String())
	}

	testCase(
		nil,
		"00",
		hashstorage.WithFileName("00"), hashstorage.WithExt("txt"),
	)

	testCase(
		hashstorage.ErrFileNameIsTooShort,
		"0",
		hashstorage.WithFileName("0"),
	)

	// Change hash func.
	s.NewHash = newBadHash
	testCase(errTestError, "")

	// Broken file dir.
	if err = os.Chmod(filepath.Join(root, "01"), 0); err != nil {
		t.Fatal(err)
	}

	testCase(
		os.ErrPermission,
		"01badpermission",
		hashstorage.WithFileName("01badpermission"),
	)

	if err = os.Chmod(filepath.Join(root, "01"), s.StoreDirPermission); err != nil { // Must to return.
		t.Fatal(err)
	}

	// Broken file.
	_, err = s.SaveFile(&badFile{}, hashstorage.WithFileName("02broken"))
	if !errors.Is(err, errTestError) {
		t.Fatal(err)
	}
}

func TestHashStorage_Open(t *testing.T) {
	t.Parallel()
	root := filepath.Join(t.TempDir(), "Open")

	s, err := hashstorage.New(root)
	if err != nil {
		t.Fatal(err)
	}

	testCase := func(wantErr error, wantContent, fileName string) {
		t.Helper()

		file, err := s.Open(fileName)
		if !errors.Is(err, wantErr) {
			t.Fatalf("wrong error:\n%v\n%v", wantErr, err)
		}

		if file == nil {
			return
		}

		defer file.Close()

		gotContent, err := io.ReadAll(file)
		if err != nil {
			t.Fatal("io.ReadAll:", err)
		}

		if wantContent != string(gotContent) {
			t.Fatalf("wrong content:\n%q\n%q", wantContent, gotContent)
		}
	}

	fileName := "deadbeaf.txt"

	testCase(os.ErrNotExist, "", fileName)

	content := "open content\n"
	_, err = s.SaveFile(bytes.NewBufferString(content), hashstorage.WithFileName(fileName))
	if err != nil {
		t.Fatal(err)
	}

	testCase(nil, content, fileName)

	testCase(hashstorage.ErrFileNameIsTooShort, "", "f")
}

func TestHashStorage_Remove(t *testing.T) {
	t.Parallel()
	root := filepath.Join(t.TempDir(), "Remove")

	s, err := hashstorage.New(root)
	if err != nil {
		t.Fatal(err)
	}

	assert := func(wantExist bool, fileName string) {
		t.Helper()

		path, err := s.GetPathToFile(fileName)
		if err != nil {
			t.Fatal("s.GetPathToFile:", err)
		}

		var gotExist bool

		_, err = os.Stat(path)
		switch {
		case err == nil:
			gotExist = true

		case errors.Is(err, os.ErrNotExist):
			gotExist = false

		default:
			t.Fatal("unexpected error:", err)
		}

		if wantExist != gotExist {
			t.Fatalf("want %v, got %v", wantExist, gotExist)
		}
	}

	fileName, err := s.SaveFile(bytes.NewBufferString("file\n"))
	if err != nil {
		t.Fatal(err)
	}

	assert(true, fileName)

	err = s.Remove(fileName)
	if err != nil {
		t.Fatal(err)
	}

	assert(false, fileName)

	err = s.Remove(fileName)
	if !errors.Is(err, os.ErrNotExist) {
		t.Fatal(err)
	}

	// Bad file name.
	err = s.Remove("f")
	if !errors.Is(err, hashstorage.ErrFileNameIsTooShort) {
		t.Fatal(err)
	}
}
